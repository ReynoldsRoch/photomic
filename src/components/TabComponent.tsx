import { IonList, IonItem, IonLabel } from "@ionic/react";

const TabComponent: React.FC = () => {
    return (
        <IonList>
            <IonItem>
                <IonLabel>Pokémon Yellow</IonLabel>
            </IonItem>
            <IonItem>
                <IonLabel>Mega Man X</IonLabel>
            </IonItem>
            <IonItem>
                <IonLabel>The Legend of Zelda</IonLabel>
            </IonItem>
            <IonItem>
                <IonLabel>Pac-Man</IonLabel>
            </IonItem>
            <IonItem>
                <IonLabel>Super Mario World</IonLabel>
            </IonItem>
            <IonItem>
                <IonLabel>Miora</IonLabel>
            </IonItem>
        </IonList>
    );
};

export default TabComponent;